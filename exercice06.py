def factorial(number):
    # Votre code ici
    if number == 0:
        return 1
    else:
        isNegative = False
        if number < 0:
            number = -number
            isNegative = True

        f = 1
        for k in range(2, number + 1):
            f = f * k

        if isNegative:
            return -f
        else:
            return f


def run():
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(4) == 24
    assert factorial(8) == 40320
    assert factorial(-8) == -40320
