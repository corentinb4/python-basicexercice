def get_letter_count(word):
    # Votre code ici
    return sum(c.isalpha() for c in word)


def run():
    assert get_letter_count("Oui") == 3
    assert get_letter_count("Bonjour") == 7
    assert get_letter_count("") == 0
    assert get_letter_count(".........hein???") == 4
    assert get_letter_count("Attention y'a quatre mots !") == 21
