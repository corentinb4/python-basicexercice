def is_number_correct(number):
    # Votre code ici oui ou non c'est entre 10 et 20. Si non renvoyer l'écart avec l'otervalle
    if 10 <= number <= 20:
        return True, 0
    elif number > 20:
        return False, -(number - 20)
    else:
        return False, 10 - number


def run():
    assert is_number_correct(0) == (False, 10)
    assert is_number_correct(10) == (True, 0)
    assert is_number_correct(20) == (True, 0)
    assert is_number_correct(21) == (False, -1)
    assert is_number_correct(50) == (False, -30)
    assert is_number_correct(15) == (True, 0)
