import pygame
from pygame import display, key
from pygame.constants import K_UP, K_DOWN, K_ESCAPE, K_z, K_s

from Ball import Ball
from GameObject import GameObject
from Paddle import Paddle


class Game(GameObject):

    def __init__(self):
        self.screenWidth = 800
        self.screenHeight = 600
        self.scorePlayer1 = 0
        self.scorePlayer2 = 0
        self.should_run = True
        self.screen = None
        self.ball = Ball(400, 300, self)
        racketWidth = 20
        racketHeight = 80
        self.racket1 = Paddle(0, self.screenHeight / 2 - racketHeight)
        self.racket2 = Paddle(self.screenWidth - racketWidth, self.screenHeight / 2 - racketHeight)

    def init(self, screen):
        self.screen = screen
        self.ball.init(screen)
        self.racket1.init(screen)
        self.racket2.init(screen)

    def update(self):
        self.displayPongInfo()
        self.displayScore()
        if (key.get_pressed()[K_ESCAPE]):
            self.should_run = False
        if (key.get_pressed()[K_z]):
            self.racket1.setY(self.racket1.y - 1)
        if (key.get_pressed()[K_s]):
            self.racket1.setY(self.racket1.y + 1)
        if (key.get_pressed()[K_UP]):
            self.racket2.setY(self.racket2.y - 1)
        if (key.get_pressed()[K_DOWN]):
            self.racket2.setY(self.racket2.y + 1)
        self.manageBallRacketsCollision()
        self.ball.update()
        self.racket1.update()
        self.racket2.update()

    def manageBallRacketsCollision(self):
        # Player 1
        if self.ball.circle.colliderect(self.racket1.rectangle):
            self.ball.inverseDirectionX()
        # Player 2
        if self.ball.circle.colliderect(self.racket2.rectangle):
            self.ball.inverseDirectionX()

    def displayPongInfo(self):
        font = pygame.font.Font('freesansbold.ttf', 32)
        info = font.render('Python Pong by Corentin', True, (255, 255, 255), (0, 0, 0))
        # set the center of the rectangular object.
        rect = info.get_rect().center = (self.screenWidth // 4, self.screenHeight // 2)
        self.screen.blit(info, rect)
        font = pygame.font.Font('freesansbold.ttf', 20)
        info = font.render('Z/S FOR PLAYER 1, UP/DOWN FOR PLAYER 2', True, (255, 255, 255), (0, 0, 0))
        # set the center of the rectangular object.
        rect = info.get_rect().center = (self.screenWidth // 4 - 20, self.screenHeight // 2 + 40)
        self.screen.blit(info, rect)

    def displayScore(self):
        font = pygame.font.Font('freesansbold.ttf', 32)
        info = font.render("{0} - {1}".format(str(self.scorePlayer1), str(self.scorePlayer2)), True, (255, 255, 255),
                           (0, 0, 0))
        # set the center of the rectangular object.
        rect = info.get_rect().center = (self.screenWidth // 2 - 40, 20)
        self.screen.blit(info, rect)
