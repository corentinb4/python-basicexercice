import random


number = input("Votre numéro (entre 0 et 49): ")
mise = input("Votre mise: ")
potentialWin = str(int(mise) * 3)

result = str(random.randint(0, 49))
print("Le numéro est {0}.".format(result))

if number == result:
    print("Gagné, vous avez le bon numéro! Vous triplez votre mise. Vous gagnez {0}€!!".format(potentialWin))
elif int(number) % 2 == int (result) % 2:
    print("Perdu, mais vous avez la bonne couleur! On vous retourne 50% votre mise. Vous gagnez {0}€!!".format(str(int(mise)/2)))
else:
    print("Perdu...")



