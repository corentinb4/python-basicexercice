import json

import requests
import pygame
from pygame import display, key
from pygame.constants import K_0, K_1, K_2, K_3, K_4, K_5, K_6, K_7, K_8, K_9, K_ESCAPE

COLORS = [
    (255, 255, 255),  # White
    (0, 0, 0),  # Black
    (255, 0, 0),  # Red
    (51, 204, 51),  # Green
    (0, 153, 255),  # Blue
    (255, 51, 204),  # Pink
    (255, 153, 0),  # Orange
    (255, 255, 0),  # Yellow
    (153, 0, 204),  # Purple
    (128, 128, 128),  # Gray
]
WINDOW_HEIGHT = 900
WINDOW_WIDTH = 900
w, h = 30, 30
GRID = [[0 for x in range(w)] for y in range(h)]
BLOCK_SIZE = 30  # Set the size of the grid block


def main():
    global SCREEN
    pygame.init()
    SCREEN = display.set_mode((900, 900))
    SCREEN.fill(COLORS[1])
    running = True
    FRAMES = 1
    getServerGrid()
    updateGrid()
    pygame.display.update()

    while running:
        FRAMES += 1
        if FRAMES % 1000000 == 0:
            getServerGrid()
            updateGrid()
            pygame.display.update()
        if key.get_pressed()[K_ESCAPE]:
            running = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if getColorFromKeyboard() is not None:
                        color(event.pos[0] // 30, event.pos[1] // 30, getColorFromKeyboard())
                updateGrid()
                pygame.display.update()


def updateGrid():
    for x in range(0, 30):
        for y in range(0, 30):
            rect = pygame.Rect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE)
            pygame.draw.rect(SCREEN, COLORS[GRID[x][y]], rect)


def getServerGrid():
    res = requests.get('http://localhost:5000/full').json()
    for x in range(0, 30):
        for y in range(0, 30):
            GRID[x][y] = res[x][y]


def color(x, y, color):
    body = {'x': x, 'y': y, 'color': color}
    headers = {'content-type': 'application/json'}
    requests.post('http://localhost:5000/place', json.dumps(body), headers=headers)
    GRID[x][y] = color


def getColorFromKeyboard():
    if key.get_pressed()[K_0]:
        return 0
    elif key.get_pressed()[K_1]:
        return 1
    elif key.get_pressed()[K_2]:
        return 2
    elif key.get_pressed()[K_3]:
        return 3
    elif key.get_pressed()[K_4]:
        return 4
    elif key.get_pressed()[K_5]:
        return 5
    elif key.get_pressed()[K_6]:
        return 6
    elif key.get_pressed()[K_7]:
        return 7
    elif key.get_pressed()[K_8]:
        return 8
    elif key.get_pressed()[K_9]:
        return 9
    else:
        return None



# On exécute la fonction main() uniquement si on lance le fichier main.py
# Si on importe le fichier main.py dans un autre fichier, __name__ ne
# sera pas égal à "__main__" et on n'exécutera pas le jeu
if __name__ == "__main__":
    main()
