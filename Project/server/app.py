import json
import os

from flask import Flask, request, abort

COLORS = [
    (255, 255, 255),  # White
    (0, 0, 0),  # Black
    (255, 0, 0),  # Red
    (51, 204, 51),  # Green
    (0, 153, 255),  # Blue
    (255, 51, 204),  # Pink
    (255, 153, 0),  # Orange
    (255, 255, 0),  # Yellow
    (153, 0, 204),  # Purple
    (128, 128, 128),  # Gray
]
w, h = 30, 30
GRID = [[0 for x in range(w)] for y in range(h)]
first_call = True
post_history = []


def init_grid():
    for x in range(0, 30):
        for y in range(0, 30):
            GRID[x][y] = 1


class MyFlaskApp(Flask):
    def run(self, host=None, port=None, debug=None, load_dotenv=True, **options):
        if not self.debug or os.getenv('WERKZEUG_RUN_MAIN') == 'true':
            with self.app_context():
                init_grid()
        super(MyFlaskApp, self).run(host=host, port=port, debug=debug, load_dotenv=load_dotenv, **options)


app = MyFlaskApp(__name__)


@app.route('/full')
def get_grid():
    return json.dumps(GRID)


@app.route('/place', methods=['POST'])
def place_cell():
    if not request.json or not 'x' in request.json or not 'y' in request.json or not 'color' in request.json:
        abort(400)
    else:
        GRID[request.json['x']][request.json['y']] = int(request.json['color'])
        post_history.append(request.json)
        return json.dumps(GRID)


@app.route('/history')
def get_history():
    return json.dumps(post_history)


app.run(host='127.0.0.1')
