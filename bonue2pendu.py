import random

wordList = ["chat", "libellule", "python", "cours", "soleil", "anticonstitutionnellement", "jazz"]
win = False
points = 8
result = list(wordList[random.randint(0, len(wordList) - 1)])
display = []

for i in range(len(result)):
    display.append("*")


def checkLetters(lettre):
    lettreReplaced = False
    for i in range(len(result)):
        if result[i] == lettre:
            display[i] = lettre
            lettreReplaced = True

    return lettreReplaced


def displayWord(w):
    toDisplay = ""
    for i in range(len(w)):
        toDisplay = toDisplay + w[i]
    print(toDisplay)


while win == False and points > 0:
    displayWord(display)
    lettre = input("Entrer une lettre: ")
    if (checkLetters(lettre) == False):
        points = points - 1
        print("Attention, il vous reste {0} points !".format(str(points)))
    if not ("*" in display):
        win = True

if (points == 0):
    print("Perdu...")

if (win == True):
    print("Gagné !")
