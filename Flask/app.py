import json
from flask import Flask, request, abort, jsonify, render_template, Response

from models.person import Person

app = Flask(__name__)
person = []


#
# EXERCICE 1
#
@app.route('/hello/', methods=['GET', 'POST'])
@app.route('/hello/<name>', methods=['GET', 'POST'])
def hello(name=None):
    return render_template('hello.html', name=name)


#
# EXERCICE 2
#

# Get all person
@app.route('/person')
def get_person():
    return json.dumps([p.__dict__ for p in person])


# Get people by id
@app.route('/person/<id>')
def get_person_by_id(id):
    l = list(filter(lambda p: p.id == id, person))
    if len(l):
        return jsonify(l[0].__dict__)
    else:
        abort(404)


# Add people
@app.route('/person', methods=['POST'])
def post_person():
    if not request.json or not 'first_name' in request.json or not 'last_name' in request.json or not 'age' in request.json:
        abort(400)
    people = Person(request.json['first_name'], request.json['last_name'], request.json['age'])
    person.append(people)
    return jsonify(people.__dict__)


# Update people
@app.route('/person/<id>', methods=['PUT'])
def update_people(id):
    li = list(filter(lambda p: p.id == id, person))
    if len(li) > 0:
        people = li[0]
        if not request.json:
            abort(400)
        if 'first_name' in request.json:
            people.first_name = request.json['first_name']
        if 'last_name' in request.json:
            people.last_name = request.json['last_name']
        if 'age' in request.json:
            people.age = request.json['age']
        try:
            index = [x.id for x in person].index(id)
            person.pop(index)
            person.append(people)
            return jsonify(people.__dict__)
        except ValueError:
            abort(404)
    else:
        abort(404)


# Delete people
@app.route('/person/<id>', methods=['DELETE'])
def delete_people(id):
    if len(person) > 0:
        try:
            index = [x.id for x in person].index(id)
            person.pop(index)
            return "OK"
        except ValueError:
            abort(404)
    else:
        abort(404)


app.run(host='127.0.0.1')
