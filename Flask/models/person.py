from uuid import uuid4


class Person:

    def __init__(self, first_name, last_name, age):
        self.id = str(uuid4())
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
